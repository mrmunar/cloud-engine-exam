<?php

namespace App\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

use App\Repository\CustomerRepository;

class CustomerController extends Controller
{
    private $customerRepository;

    public function __construct(CustomerRepository $customerRepository)
    {
        $this->customerRepository = $customerRepository;
    }

    /**
     * Retrieve all customers
     * 
     * @Route("/customer", name="getCustomer")
     * @Method({"GET"})
     * @return json
     */
    public function getCustomer()
    {
        return new JsonResponse($this->customerRepository->getCustomers());
    }

    /**
     * Creates a customer record
     * 
     * @Route("/customer", name="createCustomer")
     * @Method({"POST"})
     * @param Request $request
     * @return json
     */
    public function createCustomer(Request $request)
    {
        $fields['first_name'] = $request->request->get('first_name');
        $fields['last_name'] = $request->request->get('last_name');

        $response = $this->customerRepository->createCustomer($fields);
        return new JsonResponse($response);
    }

    /**
     * Updates a customer's first name and last name
     * 
     * @Route("/customer/{id}", name="updateCustomer")
     * @Method({"PUT"})
     * @param string $id
     * @param Request $request
     * @return json
     */
    public function updateCustomer($id, Request $request)
    {
        $fields['first_name'] = $request->request->get('first_name');
        $fields['last_name'] = $request->request->get('last_name');

        $response = $this->customerRepository->updateCustomer($id, $fields);
        return new JsonResponse($response);
    }

    /**
     * Delete a customer record
     * 
     * @Route("/customer/{id}", name="deleteCustomer")
     * @Method({"DELETE"})
     * @param string $id
     * @return json
     */
    public function deleteCustomer($id)
    {
        $response = $this->customerRepository->deleteCustomer($id);
        return new JsonResponse($response);
    }
}
