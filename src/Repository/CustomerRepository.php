<?php

namespace App\Repository;

use App\Entity\Customer;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

use Symfony\Component\Validator\Validator\ValidatorInterface;

use App\Service\MessageRenderer;

class CustomerRepository extends ServiceEntityRepository
{
    private $validator;
    private $em;
    private $messageRenderer;

    public function __construct(
        RegistryInterface $registry, 
        ValidatorInterface $validator, 
        MessageRenderer $messageRenderer
    )
    {
        parent::__construct($registry, Customer::class);
        $this->validator = $validator;
        $this->em = $this->getEntityManager();
        $this->messageRenderer = $messageRenderer;
    }

    /**
     * Retrieve all customers
     * 
     * @return array
     */
    public function getCustomers(){
        return $this->findAll();
    }

    /**
     * Creates a customer record
     * 
     * @param array $fields
     * @return array
     */
    public function createCustomer($fields){
        $Customer = new Customer;
        $Customer->setFirstName($fields['first_name']);
        $Customer->setLastName($fields['last_name']);
        $Customer->setObjectId();
        $Customer->setDateRecorded();

        $errors = $this->validator->validate($Customer);

        if(count($errors) > 0) { return $this->messageRenderer->renderErrors($errors); }

        $this->em->persist($Customer);
        $this->em->flush();

        return $this->messageRenderer->renderResponse('success', 
                $Customer->getObjectId(), 'Successfully created user!');
    }

    /**
     * Updates a customer's first name and last name
     * 
     * @param string $object_id
     * @param array $fields
     * @return array
     */
    public function updateCustomer($object_id, $fields){
        $Customer = new Customer;
        $Customer = $this->findOneBy(['object_id' => $object_id]);

        if(!$Customer) { return $this->messageRenderer
            ->renderResponse('error', $object_id, 'Customer not found!'); }

        $Customer->setFirstName($fields['first_name']);
        $Customer->setLastName($fields['last_name']);

        $errors = $this->validator->validate($Customer);

        if(count($errors) > 0) { return $this->messageRenderer->renderErrors($errors); }

        $this->em->persist($Customer);
        $this->em->flush();

        return $this->messageRenderer->renderResponse('success', 
                $Customer->getObjectId(), 'Successfully updated user!');
    }

    /**
     * Delete a customer record
     * 
     * @param string $object_id
     * @return array
     */
    public function deleteCustomer($object_id){
        $Customer = new Customer;
        $Customer = $this->findOneBy(['object_id' => $object_id]);

        if(!$Customer) { return $this->messageRenderer
            ->renderResponse('error', $object_id, 'Customer not found!'); }

        $this->em->remove($Customer);
        $this->em->flush();

        return $this->messageRenderer->renderResponse('success', 
                $object_id, 'Successfully deleted user!');
    }
}
