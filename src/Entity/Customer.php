<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CustomerRepository")
 */
class Customer
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @Assert\NotBlank(message="Please enter first name.")
     * @ORM\Column(type="string", length=255)
     */
    public $first_name;

    /**
     * @var string
     * @Assert\NotBlank(message="Please enter last name.")
     * @ORM\Column(type="string", length=255)
     */
    public $last_name;

    /**
     * @ORM\Column(type="string", length=15, unique=true)
     */
    public $object_id;

    /**
     * @ORM\Column(type="datetime")
     */
    private $date_recorded;

    public function getId(){
        return $this->id;
    }

    public function getFirstName(){
        return $this->first_name;
    }

    public function setFirstName($first_name){
        $this->first_name = $first_name;
    }

    public function getLastName(){
        return $this->last_name;
    }

    public function setLastName($last_name){
        $this->last_name = $last_name;
    }

    public function getObjectId(){
        return $this->object_id;
    }

    public function setObjectId(){
        // 15-character unique ID
        $this->object_id = substr(md5(time()),0,15);
    }

    public function getDateRecorded(){
        return $this->date_recorded;
    }

    public function setDateRecorded(){
        // Current date
        $this->date_recorded = new \Datetime("now");
    }
}
