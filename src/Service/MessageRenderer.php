<?php

namespace App\Service;

class MessageRenderer
{
    public function __construct()
    {
        // nada
    }

    /**
     * Builds a uniform response array when doing singular database transactions.
     * (depends on the frontend API consumer, though)
     * 
     * @param string $status
     * @param string $object_id
     * @param string $messgage
     * @return array
     */

    public function renderResponse($status, $object_id, $messgage)
    {
        return [$status => [
                'object_id' => $object_id,
                'message' => $messgage
            ]];
    }

    /**
     * Builds a consolidated response array based on form/input validation errors.
     * 
     * @param ConstraintViolationListInterface $errors
     * @return array
     */

    public function renderErrors($errors)
    {
        $formattedErrors['errors'] = [];
        foreach ($errors as $error) {
            array_push($formattedErrors['errors'],[
                'field' => $error->getPropertyPath(),
                'message' => $error->getMessage()
            ]);
        }
    }
}